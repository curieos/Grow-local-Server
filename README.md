# Grow.local

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/dcbc5600517643b6acb174a7c693e770)](https://app.codacy.com/manual/caroline/Grow.local-Server?utm_source=github.com&utm_medium=referral&utm_content=curieos/Grow.local-Server&utm_campaign=Badge_Grade_Settings)

Grow.local is a SEAN stack app for plant care.

## Building

Make sure you have the required dependencies installed. NodeJS and NPM are notable.

Navigate to the directory and run `npm install` to install the required npm packages

If you are building the project from source, run `npm run build` and wait for the build to complete

Finally, you can start the server by running `npm run start`
