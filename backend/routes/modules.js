const express = require('express')
const Module = require('../sequelize').Module
// const find = require('local-devices')
const Requests = require('../lib/request-wrapper')

const router = express.Router()

var moduleList = []
var rawModuleList = []

function UpdateModuleList () {
  return new Promise((resolve, reject) => {
    Module.findAll().then(modules => {
      const newList = []
      for (const module of modules) {
        const newModule = { id: module.id, name: module.name, ip: module.ip }
        newList.push(newModule)
      }
      moduleList.length = newList.length
      moduleList = [...newList]
      resolve()
    })
  })
}

function UpdateRawModuleList () {
  return new Promise((resolve, reject) => {
    Requests.NewGetRequest('new_module.local', '/module/info').then(response => {
      rawModuleList = []
      rawModuleList.push(response)
      resolve()
    }).catch(error => {
      reject(error)
    })
  })
}

router.get('', (req, res, next) => {
  UpdateModuleList().then((response) => {
    res.status(200).json({
      message: 'Modules Fetched Successfully',
      modules: moduleList
    })
  })
})

router.post('', (req, res, next) => {
  Module.create({ name: req.body.name, ip: req.body.ip }).then(module => {
    const data = JSON.stringify({
      name: module.name,
      timezoneOffset: new Date().getTimezoneOffset()
    })
    Requests.NewPostRequest(module.ip, '/module/setup', data).then(response => {
      res.status(201).json({ message: 'Successfuly Added Module' })
    }).catch(error => {
      console.error(error)
      res.status(502).json({ message: 'Failed to Setup Module' })
    })
  })
})

router.delete('/:id', (req, res, next) => {
  UpdateModuleList().then((response) => {
    /* eslint-disable eqeqeq */
    const module = moduleList.find(module => module.id == req.params.id)
    /* eslint-enable eqeqeq */
    const data = JSON.stringify({
      name: 'new_module',
      timezoneOffset: new Date().getTimezoneOffset()
    })
    Requests.NewPostRequest(`${module.name}.local`, '/module/setup', data).then(response => {
      Module.destroy({ where: { id: req.params.id } }).then(() => {
        res.status(204).json({ message: 'Successfully Deleted Module' })
      }).catch(error => {
        console.error(error)
        res.status(500).json({ message: 'Failed to Delete Module' })
      })
    }).catch(error => {
      console.error(error)
      res.status(502).json({ message: 'Failed to Reset Module' })
    })
  }).catch((error) => {
    console.error(error)
    res.status(502).json({ message: 'Failed to retrieve data from database' })
  })
})

router.get('/:id/settings', (req, res, next) => {
  UpdateModuleList().then(() => {
    /* eslint-disable eqeqeq */
    const module = moduleList.find(module => module.id == req.params.id)
    /* eslint-enable eqeqeq */
    if (typeof module === 'undefined') res.status(500).json({ message: 'Failed to find module with id' })
    else {
      res.status(200).json({
        message: 'Successfully Retrieved Module Settings',
        module: module
      })
    }
  })
})

router.post('/:id/settings', (req, res, next) => {
  Module.findOne({ where: { id: req.params.id } }).then((module) => {
    if (module === null) {
      res.status(502).json({ message: 'Failed to find module with id' })
    } else {
      module.name = req.body.name
      module.save().then(() => {
        const data = JSON.stringify({
          name: module.name,
          timezoneOffset: new Date().getTimezoneOffset()
        })
        Requests.NewPostRequest(module.ip, '/module/setup', data).then((response) => {
          res.status(200).json({ message: 'Successfully Updated Module Data' })
        })
      })
    }
  })
})

router.get('/:id/info', (req, res, next) => {
  UpdateModuleList().then(() => {
    /* eslint-disable eqeqeq */
    const module = moduleList.find(module => module.id == req.params.id)
    /* eslint-enable eqeqeq */
    Requests.NewGetRequest(`${module.name}.local`, '/module/info').then(response => {
      res.status(200).json(response)
    }).catch(error => {
      console.error(error)
      res.status(502).json({ message: 'Failed to retrieve data from module' })
    })
  }).catch((error) => {
    console.error(error)
    res.status(502).json({ message: 'Failed to retrieve data from database' })
  })
})

router.get('/raw', (req, res, next) => {
  UpdateRawModuleList().then((response) => {
    res.status(200).json({
      message: 'Raw Modules Fetched Successfully',
      modules: rawModuleList
    })
  }).catch(() => {
    console.error('Failed to find modules')
    res.status(500).json({ message: 'No modules found on network' })
  })
})

module.exports = router
