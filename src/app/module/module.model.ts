export class Module {
  id: string;
  name: string;
  moduleName: string;
  ipAddress: string;
  ambientTemperature: string;
}
